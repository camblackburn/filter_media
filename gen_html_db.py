import os 


DB_PATH = "materials"
REWRITE = True

def gen_index(indexpath, pathlist):
    '''
        makes a index page for the entire database
        just a list of links to the material files
        later update with a table summarizing the values of everything 
        filepath: path to html file created
        pathlist: list of strings that are the material pages to link to, 
            assume name from path format

    '''
    index_wd = os.path.split(indexpath)[0]
    html_file = open(indexpath, mode='w')

    content = '''
    <!DOCTYPE html>
    <html>
    <head>
        <style>
            table, th, td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <h1>Filter Media Database</h1>

        This is a tracking page to follow the filter efficiency and physical properties of various types of face mask or ventilator filter materials - a central page to compare NOISH-approved N95 masks to new materials developed by the larger CBA COVID repsonse team (<a href="https://tourlomousis.pages.cba.mit.edu/cvd-electrospinning/">electrospining</a>, <a href="https://gitlab.cba.mit.edu/camblackburn/rotary_jet_spinning">rotary jet</a> ) to DIY materials that could be easily sourced or readily available. 
        <br></br>
        With the CDC now recommending everyone uses any type of makeshift mask before going out in public, and as more people become caretakers for sick family members, we should get a quantitative measurement of which type of household items are best for transmission protection, breathability, and longenvity. 
        <br></br>
        Similar questions have been asked and answered before - <a href="https://www.researchgate.net/publication/258525804_Testing_the_Efficacy_of_Homemade_Masks_Would_They_Protect_in_an_Influenza_Pandemic">Effficiency of homemade masks</a>, <a href="https://gitlab.cba.mit.edu/pub/coronavirus/tracking/-/blob/master/docs/FilteringPerformances.pdf">filtering performance of 20 protective fabrics</a>, etc. - this projects exists to verify and expand these findings, specialize them to known <a href="https://www.biorxiv.org/content/10.1101/2020.03.08.982637v1.abstract">SARS-CoV-2 aerosol Characteristics</a>, and provide an inital comparison for novel materials. 
        <br></br>
        filtration efficiency testing is inspired by ASTM standards <a href="https://www.astm.org/Standards/F2299.htm">F2299</a> and <a href="https://www.astm.org/Standards/F2101.htm">F2101</a>, but it's NOT intended to replace or be as rigorous as NOISH testing - more details <a href="https://gitlab.cba.mit.edu/camblackburn/filter_testing">here</a>. the hope is two fold: (1) to provide convenient testing for materials developed in house before sending them to more rigorous and qualified testing labs, and (2) to provide comparative insight backed by quantitative measurement to the growing DIY mask movement. 
        <br></br>
        <br></br>
        <i>click the mask name for <b>more images & details</b></i>
    '''
    
    table = sum_table(pathlist, index_wd)
    content += table

    content = content + "\n </body>\n</html>"

    html_file.write(content)
    html_file.close()
    print("generated index file: ", indexpath)
    return

def gen_media_page(dirpath):
    '''
        generates a main page for each material studied 
        data must be stored in a txt file in the same directory 
        image folders will be scanned and images attached below text information
        dirpath: directory to media summary
    '''
    name = os.path.split(dirpath)[1]
    html_path = os.path.join(dirpath, name+".html")
    txt_path = os.path.join(dirpath, name+".txt")

    if not os.path.isfile(txt_path):
        print("text data file does not exist for %s! No html file will be added." % name)
        return

    html_file = open(html_path, mode="w")
    txt_file = open(txt_path, mode="r")

    content = '''
    <!DOCTYPE html>
    <html>
    <h1>%s</h1>
    <ul style="list-style-type:none;">
    ''' % name

    for line in txt_file:
        cat= line.split(":")[0]
        value = ":".join(line.split(":")[1:])
        if cat == "notes":
            stat = '<li><b>%s</b><ul style="list-tyle-type:none;">' % cat
            bullets = value.split(",")
            for b in bullets:
                stat += "<li>%s</li>" % b
            stat += "</ul></li>\n"
        elif "link" in cat:
            description, link = value.split(",")
            stat = '<li><b>%s</b>: <a href="%s">%s</a></li>\n' % (cat, link, description)
        else:
            stat = "<li><b>%s</b>: %s</li>\n" % (cat, value)
        content = content + stat

    for root, dirs, files in os.walk(dirpath):
        for f in files:
            base, ext = os.path.splitext(f)
            if ext in [".jpg", ".png", ".jpeg", ".tif"]:
                img_path = os.path.join(root, f)
                img_path = img_path[len(dirpath):]
                image = '<br><img src=".%s" height="400"><br>%s<br>\n' % (img_path, base)
                content += image

    content += "</html>"
    html_file.write(content)
    html_file.close()
    print("generated material page: ", html_path)
    return

def sum_table(pathlist, dir):
    ''' creates summary table of all the materials
    columns: 
        name + link to page, 
        filter efficiency, 
        pressure drop, 
        avg fiber diameter, 
        thickness, 
        weight, 
        rep image
    returns table in html string
    '''
    content = '''
    <table>
        <thead>
            <tr>
                <th>name</th>
                <th>image</th>
                <th>avg fiber diameter (um)</th>
                <th>thickness (mm)</th>
                <th>weight (g/cm^2)</th>
                <th>filter efficiency (%)</th>
                <th>pressure drop (psi)</th>
            </tr>
        </thead>
        <tbody>
    '''
    pathlist.sort()
    for path in pathlist:
        name = os.path.splitext(os.path.split(path)[1])[0]
        mat_root = os.path.split(path)[0]
        link_path = path[len(dir):]
        link = '<td><a href=".%s">%s</a></td>\n' % (link_path, name)

        effic = '<td></td>'
        pressure = '<td></td>'
        fiber = '<td></td>'
        thick = '<td></td>'
        weight = '<td></td>'
        img = '<td></td>'

        txt_path = os.path.join(mat_root, name+".txt")
        if not os.path.isfile(txt_path):
            row = "<tr>\n" + link + img + fiber + thick + weight + effic + pressure + "</tr>\n"
            content += row
            continue

        data_file = open(txt_path, "r")
        for line in data_file:
            column = line.split(":")[0]
            value = ":".join(line.split(":")[1:])
            if "efficiency" in column:
                effic = '<td>%s</td>\n' % value
            elif "pressure" in column:
                pressure = '<td>%s</td>\n' % value
            elif "fiber" in column:
                fiber = '<td>%s</td>\n' % value
            elif "thickness" in column:
                thick = '<td>%s</td>\n' % value
            elif "weight" in column:
                weight = '<td>%s</td>\n' % value
            else:
                continue
        
        SEM_path = os.path.join(mat_root, "SEM")
        if os.path.exists(SEM_path):
            SEM_imgs = os.listdir(SEM_path)
            rep_im = [img for img in SEM_imgs if "rep" in img][0]
            im_path = os.path.join(SEM_path, rep_im)[len(dir):]
            img = '<td><img src=".%s" height="200"></td>\n' % im_path

        row = "<tr>\n" + link + img + fiber + thick + weight + effic + pressure + "</tr>\n"
        content += row
    
    content += "</tbody>"
    return content


cwd = os.getcwd()
DB_PATH = os.path.join(cwd, DB_PATH)
INDEX_PATH = cwd

if not os.path.exists(DB_PATH):
    os.mkdir(DB_PATH)

html_media_pages = []
for root, dirs, files in os.walk(DB_PATH):
    for name in dirs:
        if root.split("\\") > DB_PATH.split("\\"):
            continue
        if not os.path.isfile(os.path.join(cwd, name, name+".html")) or REWRITE:
            gen_media_page(os.path.join(root, name))
        html_media_pages.append(os.path.join(root, name, name+".html"))

# make index file if it doesn't exist
index = os.path.join(INDEX_PATH, "index.html")
if not os.path.isfile(index) or REWRITE:
    gen_index(index, html_media_pages)
