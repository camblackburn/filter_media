name: 3M 300 MPR
manufacture link: Clean Living Filtrete, https://www.filtrete.com/3M/en_US/filtrete/products/~/Filtrete-Clean-Living-Air-Filters/?N=4315+3292676072+3294529207&preselect=7568680+7570556+3293786499&rt=rud
thickness (mm): 0.292
weight (g/cm^2):
notes: MERV 5 rating, $5.59 for 40x50cm of material
