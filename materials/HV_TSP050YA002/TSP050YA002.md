# H&V TSP050YA002

manufacturer site: [HVAC](https://www.hollingsworth-vose.com/en/Products/Filtration-Media/Air-Filtration1/HVAC-filtration/)

## measurements
- thickness: 0.446 mm
- weight (10x10mm square): 0.0143 g

## imaging 
- couldn't reach high magnification without getting over charging and split horizontal lines
- small circles were clearly pressed from the fabric, but the larger circles
show no sign of fiber strands

<img src="./SEM/TSP050YA002_20nmAu_top_0007.jpg" height="600">

<img src="./SEM/TSP050YA002_20nmAu_top_0009.jpg" height="600">

<img src="./SEM/TSP050YA002_20nmAu_top_0006.jpg" height="600">

<img src="./SEM/TSP050YA002_20nmAu_top_0008.jpg" height="600">

<img src="./SEM/TSP050YA002_20nmAu_top_0010.jpg" height="600">
