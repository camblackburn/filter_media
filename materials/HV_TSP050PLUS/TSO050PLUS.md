# H&V TSP050PLUS

manufacturer site: [PN13015AP2](https://www.hollingsworth-vose.com/en/Products/Filtration-Media/Air-Filtration1/HVAC-filtration/)

## measurements
- thickness: 0.295 mm
- weight (10x10mm square): 0.0064 g

## imaging 
<img src="./SEM/TS050PLUS_20nmAu_top_0001.jpg" height="600">

<img src="./SEM/TS050PLUS_20nmAu_top_0003.jpg" height="600">

<img src="./SEM/TS050PLUS_20nmAu_top_0001.jpg" height="600">

<img src="./SEM/TS050PLUS_20nmAu_top_0005.jpg" height="600">

<img src="./SEM/TS050PLUS_20nmAu_top_0006.jpg" height="600">
