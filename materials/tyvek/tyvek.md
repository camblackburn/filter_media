# Tyvek - FedEx shipping envelope

manufacturer site: [FedEx shipping Envelope](http://www.fedex.com/fi_english/shippingguide/packaging.html)

## measurements
- thickness: 0.143 mm
- weight (10x10mm square): 0.0065 g

## imaging 
<img src="./SEM/tyvek_20nmAu_top_0007.jpg" height="600">

<img src="./SEM/tyvek_20nmAu_top_0001.jpg" height="600">

<img src="./SEM/tyvek_20nmAu_top_0002.jpg" height="600">

<img src="./SEM/tyvek_20nmAu_top_0005.jpg" height="600">

<img src="./SEM/tyvek_20nmAu_top_0006.jpg" height="600">
