# Arauco 

## BKP 
manufacturer page: [Arauco BKP](https://www.arauco.cl/na/marcas/woodpulp/arauco-bkp/)

<img src="./BKP/bkp_raw.jpg" height = "600">

### physical measurements 
the pieces gold coated were imaged.

<img src="./BKP/bkp_measured.jpg" height = "600">

### imaging
#### 80% compressed
<img src="./BKP/SEM/AraucoBKP_1_20nmAu_top_0002_raw.jpg" height="600">

<img src="./BKP/SEM/AraucoBKP_1_20nmAu_top_0005_raw.jpg" height="600">

<img src="./BKP/SEM/AraucoBKP_1_20nmAu_top_0006_raw.jpg" height="600">

#### 20% compressed
top view from #6

<img src="./BKP/SEM/AraucoBKP_6_20nmAu_top_0001_raw.jpg" height="600">

<img src="./BKP/SEM/AraucoBKP_6_20nmAu_top_0003_raw.jpg" height="600">

side view from # 7
- you can see the layers that were more or less compressed

<img src="./BKP/SEM/AraucoBKP_7_20nmAu_side_0001.jpg" height="600">

<img src="./BKP/SEM/AraucoBKP_7_20nmAu_side_0003.jpg" height="600">

<img src="./BKP/SEM/AraucoBKP_7_20nmAu_side_0004.jpg" height="600">

<img src="./BKP/SEM/AraucoBKP_7_20nmAu_side_0007.jpg" height="600">



## EKP
manufacturer page: [Arauco EKP](https://www.arauco.cl/aus-nz/marcas/woodpulp/arauco-ekp/)

<img src="./ekp_raw.jpg" height = "600">

### physical measurements
<img src="./EKP/ekp_measured.jpg" height = "600">

### imaging
#### 80% compressed
<img src="./EKP/SEM/AraucoEKP_1_20nmAu_top_0009_raw.jpg" height="600">

<img src="./EKP/SEM/AraucoEKP_1_20nmAu_top_0011.jpg" height="600">

<img src="./EKP/SEM/AraucoEKP_1_20nmAu_top_0013.jpg" height="600">

<img src="./EKP/SEM/AraucoEKP_1_20nmAu_top_0015.jpg" height="600">

#### 20% compressed
<img src="./EKP/SEM/AraucoEKP_8_20nmAu_top_0001.jpg" height="600">

<img src="./EKP/SEM/AraucoEKP_8_20nmAu_top_0004.jpg" height="600">

<img src="./EKP/SEM/AraucoEKP_8_20nmAu_top_0001.jpg" height="600">

there are small, leaf-like features with tiny holes . . 

<img src="./EKP/SEM/AraucoEKP_8_20nmAu_top_0005.jpg" height="600">

<img src="./EKP/SEM/AraucoEKP_8_20nmAu_top_0007.jpg" height="600">

<img src="./EKP/SEM/AraucoEKP_8_20nmAu_top_0008.jpg" height="600">

## UKP
manufacturer page: [Arauco UKP](https://www.arauco.cl/na/marcas/woodpulp/arauco-ukp/)

<img src="./ukp_raw.jpg" height = "600">

### physical measurements
<img src="./UKP/ukp_measured.jpg" height = "600">

### imaging
#### 80% compressed
<img src="./UKP/SEM/AraucoUKP_1_20nmAu_top_0001.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_1_20nmAu_top_0002.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_1_20nmAu_top_0005.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_1_20nmAu_top_0006.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_1_20nmAu_top_0003.jpg" height="600">

#### 20% compressed

<img src="./UKP/SEM/AraucoUKP_6_20nmAu_top_0001.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_6_20nmAu_top_0003.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_6_20nmAu_top_0005.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_6_20nmAu_top_0007.jpg" height="600">

<img src="./UKP/SEM/AraucoUKP_6_20nmAu_top_0008.jpg" height="600">
