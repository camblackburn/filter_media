# Gerson N95 Mask 

manufacterer site: [1730 N95 Particulate Respirator](https://www.gersonco.com/product/1730-n95-particulate-respirator/)

- electrostatically-charged (is this for filter efficiency or breathing easability?)
- nose cushion on the inside for comfort

<img src="./gersonN95.jpg" height="600">

three layer mask (inside layer --> outside layer) 

<img src="./gersonN95_layers.jpg" height="600">


## physical measurements
#### thickness (mm pm 0.005) measurements: 
- overall: 1.076
- inside layer: 0.495
- middle layer: 0.217
- outside layer: 0.429

#### weight (10(pm 0.5) x10(pm 0.5) mm square material) measured in g pm 0.0005
- overall: 0.0306
- inside layer: 0.0120
- middle layer: 0.0051
- outside layer: 0.0122

## imaging 

### SEM
no sputter coating 

#### inside layer
<img src="./SEM/Gerson95_nosputter_insidelayer_0001.jpg" height="600">

<img src="./SEM/Gerson95_nosputter_insidelayer_0003_raw.jpg" height="600">

#### middle layer
<img src="./SEM/Gerson95_nosputter_middlelayer_0002.jpg" height="600">

<img src="./SEM/Gerson95_nosputter_middlelayer_0004.jpg" height="600">

#### outside layer
<img src="./SEM/Gerson95_nosputter_outsidelayer_0001.jpg" height="600">

<img src="./SEM/Gerson95_nosputter_outsidelayer_0004_raw.jpg" height="600">

cuts cleanly - outside layer edge

<img src="./SEM/Gerson95_nosputter_outsidelayer_0002_raw.jpg" height="600">

### Optical 
these are pretty 

<img src="./optical/outsidelayer_3.jpg" height="600">

<img src="./optical/outsidelayer_4.jpg" height="600">

<img src="./optical/outsidelayer_5.jpg" height="600">
