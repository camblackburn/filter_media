name: Gerson_N95
manufacture link: 1730 N95 Particulate Respirator, https://www.gersonco.com/product/1730-n95-particulate-respirator/
thickness (mm): 1.076
weight (g/cm^2): 0.0306
notes: electrostatically charged (for filter efficiency or easy breathing?), nose cushion on the inside for comfort, did not sputter coat for SEM images
