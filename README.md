# [Characterizing Filter media](https://camblackburn.pages.cba.mit.edu/filter_media/)
<b> database [HERE](https://camblackburn.pages.cba.mit.edu/filter_media/) </b>

This is a tracking page to follow the filter efficiency and physical properties of various types of face mask or ventilator filter materials - a central page to compare NOISH-approved N95 masks to new materials developed by the larger CBA COVID repsonse team ([electrospining](https://tourlomousis.pages.cba.mit.edu/cvd-electrospinning/), [rotary jet](https://gitlab.cba.mit.edu/camblackburn/rotary_jet_spinning) ) to DIY materials that could be easily sourced or readily available. 

With the CDC now recommending everyone uses any type of makeshift mask before going out in public, and as more people become caretakers for sick family members, we should get a quantitative measurement of which type of household items are best for transmission protection, breathability, and longenvity. 

Similar questions have been asked and answered before - [Effficiency of homemade masks](https://www.researchgate.net/publication/258525804_Testing_the_Efficacy_of_Homemade_Masks_Would_They_Protect_in_an_Influenza_Pandemic), [filtering performance of 20 protective fabrics](https://gitlab.cba.mit.edu/pub/coronavirus/tracking/-/blob/master/docs/FilteringPerformances.pdf), etc. - this projects exists to verify and expand these findings, specialize them to known [SARS-CoV-2 aerosol Characteristics](https://www.biorxiv.org/content/10.1101/2020.03.08.982637v1.abstract), and provide an inital comparison for novel materials. 

filtration efficiency testing is inspired by ASTM standards [F2299](https://www.astm.org/Standards/F2299.htm) and [F2101](https://www.astm.org/Standards/F2101.htm), but it's NOT intended to replace or be as rigorous as NOISH testing - more details [here](https://gitlab.cba.mit.edu/camblackburn/filter_testing). the hope is two fold: (1) to provide convenient testing for materials developed in house before sending them to more rigorous and qualified testing labs, and (2) to provide comparative insight backed by quantitative measurement to the growing DIY mask movement. 



## Measurement methods
- filter material width: micrometer, squeezed until width is no longer changing 
- fiber thickness: SEM, data measurement 